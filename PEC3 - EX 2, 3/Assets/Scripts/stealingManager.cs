using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class stealingManager : MonoBehaviour
{
    public TextMeshProUGUI coinsCountText;
    public bool canAddCoin;
    public bool isStealing;
    public int coinsCount;
    // Inicializa los booleanos para poder robar monedas.
    void Start()
    {
        canAddCoin = true;
        isStealing = false;
    }
    // A�ade una moneda al contador y empieza la rutina stealRoutine.
    public void addCoin()
    {
        StartCoroutine(stealRoutine());
        coinsCount += 1;
        coinsCountText.SetText("" + coinsCount);
    }
    // Rutina que cambia  el valor de los booleanos para impedir robar hasta pasados dos segundos.
    IEnumerator stealRoutine()
    {
        canAddCoin = false;
        isStealing = true;
        yield return new WaitForSeconds(2);
        isStealing = false;
        canAddCoin = true;
    }
}
