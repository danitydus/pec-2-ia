using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
public class Basurero : Agent
{
    Rigidbody rBody;

    public GameObject basura;
    public GameObject papelera;
    public float moveSpeed;
    public bool papeleraTime;
    private Vector3 basuraPosition;
    public Transform fountain;
    public float distanceToTarget;

    void Start()
    {
        papeleraTime = false;
        rBody = GetComponent<Rigidbody>();
        papelera.SetActive(true);
        basura.SetActive(true);
    }
    // Cuando empieza un episodio, tanto el basurero como el objetivo se inicializan en un punto aleatorio de la plaza.
    public override void OnEpisodeBegin()
    {
        if (this.transform.localPosition.y < 0)
        {
            this.rBody.angularVelocity = Vector3.zero;
            this.rBody.velocity = Vector3.zero;
            this.transform.localPosition = new Vector3(Random.Range(12.5f, -15f),
                                           3.63f, Random.Range(9f, -17f));
        }
        basura.transform.localPosition = new Vector3(Random.Range(12.5f, -15f),
                                           3.63f, Random.Range(9f, -17f));
    }
    // Durante todo el tiempo de ejecuci�n el basurero mira hacia el objetivo ( que dependiendo el momento
    //  puede ser la basura o la papelera ). 
    public void Update()
    {
        basuraPosition = new Vector3(this.transform.localPosition.x + 0.25f, this.transform.localPosition.y, this.transform.localPosition.z + 0.7f);
        if (papeleraTime == true)
        {
            basura.transform.localPosition = basuraPosition;
            SetReward(-0.001f);
        }
        if (papeleraTime == false)
        {
            this.transform.LookAt(basura.transform);
        }
        else
        {
            this.transform.LookAt(papelera.transform);
        }
        distanceToTarget = Vector3.Distance(basura.transform.localPosition, this.transform.localPosition);
     
    }
    // Durante el entrenamiento se recogen observaciones sobre las posiciones del basurero, del objetivo, y de la fuente central.

    public override void CollectObservations(VectorSensor sensor)
    {
        // Target and Agent positions
        sensor.AddObservation(Vector3.Distance(papelera.transform.localPosition, this.transform.localPosition));
        sensor.AddObservation((papelera.transform.localPosition - this.transform.localPosition).normalized);
        sensor.AddObservation(Vector3.Distance(basura.transform.localPosition, this.transform.localPosition));
        sensor.AddObservation((basura.transform.localPosition - this.transform.localPosition).normalized);
        sensor.AddObservation(papeleraTime);
        sensor.AddObservation(Vector3.Distance(fountain.localPosition, this.transform.localPosition));
        sensor.AddObservation((fountain.localPosition - this.transform.localPosition).normalized);
        // Agent velocity
        sensor.AddObservation(rBody.velocity.x);
        sensor.AddObservation(rBody.velocity.z);
    }
    // Si el basurero cae de la plaza durante el entrenamiento, tendr� una penalizaci�n y se reiniciar� el episodio.
    public override void OnActionReceived(ActionBuffers actionBuffers)
    {
        float moveX = actionBuffers.ContinuousActions[0];
        float moveZ = actionBuffers.ContinuousActions[1];
        transform.position += new Vector3(moveX, 0, moveZ) * Time.deltaTime * moveSpeed;
        // Fell off platform
        if (this.transform.localPosition.y < 0)
        {
            SetReward(-1f);
            EndEpisode();
        }
    }
    // Si el basurero llega al objetivo, dependiendo del valor del boleano papeleraTime tendr� una recompensa positiva y se reiniciara el episodio.
    // Si por lo contrario choca con un obstaculo, tendra una peque�a penalizaci�n y se reiniciar� el episodio.
    private void OnTriggerStay(Collider Other)
    {
        // Reached target
        if (Other.tag == "Papelera" && papeleraTime == true)
        {
            FindObjectOfType<camManager>().basurasTotal += 1;
            papeleraTime = false;
            SetReward(+1f);
            EndEpisode();
        }
        else if (Other.tag == "Basura" && papeleraTime == false)
        {
            papeleraTime = true;
            SetReward(+1f);
            EndEpisode();
        }
        else if (Other.tag == "Basura" && papeleraTime == true)
        {
            SetReward(-0.001f);
        }
        else if (Other.tag == "Papelera" && papeleraTime == false)
        {
            SetReward(-0.001f);
        }
        else if (Other.tag == "Fountain")
        {
            SetReward(-0.5f);
           
        }
        else if (Other.tag == "Banco")
        {
            SetReward(-0.1f);
          
        }else if (Other.tag == "Arbol")
        {
            SetReward(-0.1f);
        }
    }
    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> continuousActions = actionsOut.ContinuousActions;
        continuousActions[0] = Input.GetAxisRaw("Horizontal");
        continuousActions[1] = Input.GetAxisRaw("Vertical");
    }
}
