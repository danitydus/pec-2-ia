using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace BBUnity.Actions
{
    /// Acci�n que permite robar una moneda cada 2 segundos.
    [Action("Stealing")]
    [Help("")]
    public class Stealing : GOAction
    {
        private UnityEngine.AI.NavMeshAgent navAgent;
        private bool canSteal;
      // Siempre que se permita robar (cada 2 segundos), se llamar� a la acci�n canAddCoin para robar una moneda m�s.
        public override TaskStatus OnUpdate()
        {
            
            if (gameObject.GetComponent<stealingManager>().canAddCoin == true) {
                gameObject.GetComponent<stealingManager>().addCoin();
                
            }
            return TaskStatus.RUNNING;
        }
    }
}
