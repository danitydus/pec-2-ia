using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class monitorManager : MonoBehaviour
{
    private Animator anim;
    public bool canSwitch;
    public List<GameObject> pupils = new List<GameObject>();

    // Se inicializa el animador del monitor y se permite empezar a cambiar de posici�n.
    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        canSwitch = true;
    }
    // Si el booleano canSwitch lo permite se empezar� la rutina que permite el cambio de pose.
    void Update()
    {
        if (canSwitch == true)
        {
            canSwitch = false;
            StartCoroutine(switchPoseRoutine());
        }
    }
    // El animador del monitor empieza a ejecutar una pose aleatorio ( de un total de 5 ), durante una franja de tiempo entre 3 y 7 segundos.
    IEnumerator switchPoseRoutine()
    {
        int posId;
        posId = Random.Range(1,5);
        if (posId == 1)
        {
            anim.SetTrigger("Pose1");
        }
        else if (posId == 2)
        {
            anim.SetTrigger("Pose2");
        }
        else if (posId == 3)
        {
            anim.SetTrigger("Pose3");
        }else if (posId == 4)
        {
            anim.SetTrigger("Pose4");
        }else if (posId == 5)
        {
            anim.SetTrigger("Pose5");
        }
        teachPupils(posId);
        yield return new WaitForSeconds(Random.Range(3, 7));
        canSwitch = true;
    }
    // Se invoca la accion switchPose de cada alumno para que estos cambien a la misma pose del monitor.
    public void teachPupils(int posId)
    {
        for (int i = 0; i < pupils.Count; i++)
        {
            pupils[i].GetComponent<alumnoManager>().switchPosition(posId);
        }
    }
}
