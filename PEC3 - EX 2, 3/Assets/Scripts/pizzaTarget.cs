using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pizzaTarget : MonoBehaviour
{
   
    // Durante el entrenamiento, si el objetivo toca la valla, el banco o el arbol, se vuelve a inicializar en otra
    // posici�n aleatoria.
    private void OnTriggerStay(Collider Other)
    {
         if (Other.gameObject.tag == "Valla")
        {
            this.transform.localPosition = new Vector3(Random.Range(4f, +31f),
                                           1.7f, Random.Range(24f, 0f));
        }
        else if (Other.gameObject.tag == "Banco")
        {

            this.transform.localPosition = new Vector3(Random.Range(4f, +31f),
                                           1.7f, Random.Range(24f, 0f));
        }
        else if (Other.gameObject.tag == "Arbol")
        {

            this.transform.localPosition = new Vector3(Random.Range(4f, +31f),
                                           1.7f, Random.Range(24f, 0f));
        }
       
    }
}
