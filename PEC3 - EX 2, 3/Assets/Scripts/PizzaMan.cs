using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using TMPro;
public class PizzaMan : Agent
{
    Rigidbody rBody;
    public Transform Target;
    public float moveSpeed;
    public Transform fountain;
    public Transform fountainTarget;
    public float distanceToTarget;

    public bool obstacle;
    void Start()
    {
        rBody = GetComponent<Rigidbody>();
    }

    // Durante todo el tiempo de ejecuci�n el pizzero mira hacia el objetivo. Adem�s, se calcula
    // la distancia entre el objetivo y el centro de la fuente para evitar que se inicialize encima de ella.
    void Update()
    {
    distanceToTarget = Vector3.Distance(Target.localPosition, fountainTarget.localPosition);
        if (distanceToTarget < 5f)
        {
            EndEpisode();
        }
        transform.LookAt(Target);
      

    }
    // Cuando empieza un episodio, tanto el objetivo como el pizzero se inicializan en un punto aleatorio de la plaza.
    public override void OnEpisodeBegin()
    {  
        Target.localPosition = new Vector3(Random.Range(4f, +31f),
                                           1.7f, Random.Range(24f, 0f));

    }
    // Durante el entrenamiento se recogen observaciones sobre las posiciones del pizzero, del objetivo, y de la fuente central.
    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(Vector3.Distance(Target.localPosition, this.transform.localPosition));
        sensor.AddObservation((Target.localPosition - this.transform.localPosition).normalized);
        sensor.AddObservation(Vector3.Distance(fountain.localPosition, this.transform.localPosition));
        sensor.AddObservation((fountain.localPosition - this.transform.localPosition).normalized);
        sensor.AddObservation(this.transform.forward);


        sensor.AddObservation(rBody.velocity.x);
        sensor.AddObservation(rBody.velocity.z);
    }
    // Si el pizzero cae de la plaza durante el entrenamiento, tendr� una penalizaci�n y se reiniciar� el episodio.
    public override void OnActionReceived(ActionBuffers actions)
    {
        float moveX = actions.ContinuousActions[0];
        float moveZ = actions.ContinuousActions[1];

        
        transform.position += new Vector3(moveX, 0, moveZ) * Time.deltaTime * moveSpeed;
        if (this.transform.localPosition.y < 0 || this.transform.localPosition.y > 3)
        {
            SetReward(-1f);
            this.transform.localPosition = new Vector3(Random.Range(4.7f, +24f),
                                           1.7f, Random.Range(-2f, +21f));
            FindObjectOfType<camManager>().pizzasTotal = 0;
            EndEpisode();
        }
    }
    // Si el pizzero llega al objetivo, tendr� una recompensa positiva y se reiniciara el episodio. Si por lo contrario choca
    // con un obstaculo, tendra una peque�a penalizaci�n y se reiniciar� el episodio.
    private void OnTriggerStay(Collider Other)
    {
        if (Other.tag == "PizzaTarget")
        {
            SetReward(+1f);
            FindObjectOfType<camManager>().pizzasTotal += 1;
             EndEpisode();
        }
        else if (Other.tag == "Arbol")
        {
          
            SetReward(-0.05f);
         

        }
        else if(Other.tag == "Banco")
        {
            SetReward(-0.05f);
        
        }
        else if(Other.tag == "Fountain")
        {
            SetReward(-0.05f);
          
        }


    } private void OnTriggerEnter(Collider Other)
    {
        obstacle = true;
    }private void OnTriggerExit(Collider Other)
    {
        obstacle = false;
    }
    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> continuousActions = actionsOut.ContinuousActions;
        continuousActions[0] = Input.GetAxisRaw("Horizontal");
        continuousActions[1] = Input.GetAxisRaw("Vertical");
    }

}
