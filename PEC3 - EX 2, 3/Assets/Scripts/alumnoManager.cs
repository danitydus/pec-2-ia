using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class alumnoManager : MonoBehaviour
{
    private Animator anim;

    // Se inicializa el animador del alumno.
    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
    }

    // Acci�n publica que se llama desde el monitor que empieza el cambio de posici�n.
    public void switchPosition(int posId)
    {
        StartCoroutine(switchPoseRoutine(posId));
    }
    // En una corta franja entre 0 y 0.7 segundos, el alumno empieza a animar la posici�n que le indica el monitor.
    IEnumerator switchPoseRoutine(int posId)
    {
        yield return new WaitForSeconds(Random.Range(0, 0.7f));

        if (posId == 1)
        {
            anim.SetTrigger("Pose1");
        }
        else if (posId == 2)
        {
            anim.SetTrigger("Pose2");
        }
        else if (posId == 3)
        {
            anim.SetTrigger("Pose3");
        }
        else if (posId == 4)
        {
            anim.SetTrigger("Pose4");
        }
        else if (posId == 5)
        {
            anim.SetTrigger("Pose5");
        }
    }
}
