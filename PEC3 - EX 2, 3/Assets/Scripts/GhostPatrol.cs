using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class GhostPatrol : MonoBehaviour
{
    // Las variables que necesita el agente fantasma: las coordenadas de posici�n que
    // indican el destino del fantasma, y el agente runner que le seguir�.
    public Vector3 destino;
    public GameObject runner;
    // La velocidad del agente fantasma tendr� siempre un valor aleatorio entre unos
    // m�rgenes definidos, al igual que su destino inicial, que siempre estar� dentro
    // de los l�mites del escenario.
    void Awake()
    {
        NavMeshAgent agente = GetComponent<NavMeshAgent>();
        agente.speed = 0.4f;
        destino = new Vector3(Random.Range(3.6f, -25), 1.64f, Random.Range(-19.7f, 8.9f));
    }
    // Durante todo momento el destino del agente fantasma ser� el equivalente al de la
    // variable destino. Esta cambiar� por otro valor aleatorio (dentro de
    // los m�rgenes del escenario) siempre que el agente haya llegado a su destino.
    // Adem�s, cuando el fantasma se distancie demasiado del runner se detendr� hasta
    // que la distancia vuelva a reducirse.
    void Update()
    {
        NavMeshAgent agente = GetComponent<NavMeshAgent>();
        agente.destination = destino;

        if (agente.remainingDistance <= agente.stoppingDistance)
        {
            destino = new Vector3(Random.Range(3.6f, -25), 1.64f, Random.Range(-19.7f, 8.9f));
        }
       if ( Vector3.Distance(transform.position, runner.transform.position) > 5){
            agente.isStopped = true;
        }else
        {
            agente.isStopped = false;
        }
    }
}
