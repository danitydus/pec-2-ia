using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
public class RollerAgent : Agent
{
    Rigidbody rBody;
    public Transform Target;
    public float forceMultiplier = 10;
    public Material winMaterial;
    public Material loseMaterial;
    public MeshRenderer floorMeshRenderer;

    public GameObject basura;
    public GameObject papelera;
    public float moveSpeed;
    public bool papeleraTime;
    private Vector3 basuraPosition;
    void Start()
    {
        papeleraTime = false;
        rBody = GetComponent<Rigidbody>();
        papelera.SetActive(true);
        basura.SetActive(true);
    }
    public override void OnEpisodeBegin()
    {
        // If the Agent fell, zero its momentum
        if (this.transform.localPosition.y < 0)
        {
            this.rBody.angularVelocity = Vector3.zero;
            this.rBody.velocity = Vector3.zero;
            this.transform.localPosition = new Vector3(0, 0.5f, 0);
        }
        // Move the target to a new spot
        basura.transform.localPosition = new Vector3(Random.value * 8 - 4,
                                           0.5f,
                                           Random.value * 8 - 4);
       
    }
    public void Update()
    {
        basuraPosition = new Vector3(this.transform.localPosition.x + 0.25f, this.transform.localPosition.y, this.transform.localPosition.z + 0.7f);
        if (papeleraTime == true)
        {
            basura.transform.localPosition = basuraPosition;
        }
        if (papeleraTime == false)
        {
            this.transform.LookAt(basura.transform);
        }
        else
        {
            this.transform.LookAt(papelera.transform);

        }
    }
    public override void CollectObservations(VectorSensor sensor)
    {
        // Target and Agent positions
        sensor.AddObservation(papelera.transform.localPosition);
        sensor.AddObservation(papeleraTime);
        sensor.AddObservation(basura.transform.localPosition);
        sensor.AddObservation(this.transform.localPosition);

        // Agent velocity
        sensor.AddObservation(rBody.velocity.x);
        sensor.AddObservation(rBody.velocity.z);
    }
    public override void OnActionReceived(ActionBuffers actionBuffers)
    {
        float moveX = actionBuffers.ContinuousActions[0];
        float moveZ = actionBuffers.ContinuousActions[1];


        transform.position += new Vector3(moveX, 0, moveZ) * Time.deltaTime * moveSpeed;
        // Fell off platform
        if (this.transform.localPosition.y < 0)
        {
            SetReward(-1f);
            floorMeshRenderer.material = loseMaterial;
            EndEpisode();
        }
    }
    // Rewards
    private void OnTriggerStay(Collider Other)
    {
        // Reached target
        if (Other.tag == "Papelera" && papeleraTime == true)
        {
            papeleraTime = false;
            SetReward(+1f);
            floorMeshRenderer.material = winMaterial;
            EndEpisode();
        }else if (Other.tag == "Basura" && papeleraTime == false)
        {
            papeleraTime = true;
            SetReward(+1f);
            floorMeshRenderer.material = winMaterial;
            EndEpisode();
        }else if (Other.tag == "Basura" && papeleraTime == true)
        {
            SetReward(-0.05f);
        }else if (Other.tag == "Papelera" && papeleraTime == false)
        {
            SetReward(-0.05f);
        }
    }
    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> continuousActions = actionsOut.ContinuousActions;
        continuousActions[0] = Input.GetAxisRaw("Horizontal");
        continuousActions[1] = Input.GetAxisRaw("Vertical");
    }
}
